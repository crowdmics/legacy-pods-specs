Pod::Spec.new do |s|
s.name         = "CMManager"
s.version      = "0.0.22"
s.summary      = "Crowd Mics SDK"
s.homepage     = "http://www.crowdmics.com"
s.author       = { "Adam Gessel" => "adam@crowdmics.com" }
s.platform     = :ios, '8.0'
s.source       = {:git => 'https://bitbucket.org/crowdmics/cmmanager.git'}
s.source_files  = '**/*.{h,cpp}'
s.preserve_paths = 'libCMManagerFramework.a'
s.library = 'CMManagerFramework'
s.dependency 'jrtplib'
s.dependency 'NSData+Base64'
s.dependency 'Reachability'
s.dependency 'AppleGuice'
s.dependency 'AFNetworking'
s.dependency 'DXDomain'
s.dependency 'DXUIKit'
s.dependency 'DXDAL'
s.dependency 'DXTableFramework'
s.dependency 'CocoaAsyncSocket'
s.dependency 'DXFoundation'
s.dependency 'UITableView-NXEmptyView'
s.dependency 'V8HorizontalPickerView'
s.dependency 'MCUIColorUtils'
s.dependency 'DXValidator'
s.dependency 'EDQueue'
s.dependency 'ISDiskCache'
end
