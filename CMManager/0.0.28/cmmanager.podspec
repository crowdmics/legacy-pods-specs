Pod::Spec.new do |s|
s.name         = "CMManager"
s.version      = "0.0.28"
s.summary      = "Crowd Mics SDK"
s.homepage     = "http://www.crowdmics.com"
s.author       = { "Adam Gessel" => "adam@crowdmics.com" }
s.platform     = :ios, '8.0'
s.source       = {:git => 'https://bitbucket.org/crowdmics/cmmanager.git'}
s.source_files  = '**/*.{h,cpp}'
s.preserve_paths = 'libCMManagerFramework.a'
s.library = 'CMManagerFramework'
s.dependency 'jrtplib'
s.dependency 'NSData+Base64'
s.dependency 'TMReachability'
s.dependency 'AppleGuice'
s.dependency 'AFNetworking', '~> 1.0'
s.dependency 'DXDomain'
s.dependency 'DXDAL'
s.dependency 'CocoaAsyncSocket'
s.dependency 'DXFoundation'
s.dependency 'DXValidator'
s.dependency 'EDQueue'
s.dependency 'ISDiskCache'
s.dependency 'JTObjectMapping-Fork'
s.dependency 'JSONKit', "1.5noerr"
end
