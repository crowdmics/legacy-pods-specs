Pod::Spec.new do |s|
   s.name   = 'DXTableFramework'
   s.version   = '0.0.6'
   
   s.platform = :ios, '5.0'
   s.summary   = 'For creating custom tableViews.'
   s.homepage  = 'http://111minutes.com/'
   s.author = "111minutes"
   s.license   = "Apache License, Version 2.0"
   s.source = { :git => 'git@bitbucket.org:crowdmics/dxtableframework.git', :tag => '0.0.6' }
   s.source_files = 'DXTableKit/Code/**/*.{h,m}'
   s.requires_arc = true

   s.dependency     'DXFoundation'
   s.dependency     'MBProgressHUD'
   s.dependency     'PSTCollectionView'
end
