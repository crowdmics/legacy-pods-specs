Pod::Spec.new do |s|
s.name         = "jrtplib"
s.version      = "0.0.1"
s.summary      = "Wraps RTP"
s.homepage     = "http://www.crowdmics.com"
s.author       = { "Adam Gessel" => "adam@crowdmics.com" }
s.platform     = :ios, '8.0'
s.source       = {:git => 'https://bitbucket.org/crowdmics/jrtplib.git'}
s.source_files  = 'src/**/*.{h,cpp}'
s.library = 'c++'
s.xcconfig = {
   'CLANG_CXX_LANGUAGE_STANDARD' => 'c++11',
   'CLANG_CXX_LIBRARY' => 'libc++'
}
end
